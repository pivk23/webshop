import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";

import App from "./modules/App/App";

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>    
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);
