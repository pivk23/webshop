import React from "react";

import { connect } from "react-redux";
import { addItem } from "../../../redux/cart/cart-actions.js";

import { SubmitButton } from "../../SignIn-SignUp/components";

import "../styles/collection-item.scss";

const CollectionItem = ({ item, addItem }) => {
  const { name, price, imageUrl } = item;
  return (
    <div className="collection-item">
      <div
        className="image"
        style={{
          backgroundImage: `url(${imageUrl})`,
        }}
      />
      <div className="collection-footer">
        <span className="name">{name}</span>
        <span className="price">{price}</span>
      </div>
      <SubmitButton inverted onClick={() => addItem(item)}>
        ADD TO CART
      </SubmitButton>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  addItem: (item) => dispatch(addItem(item)),
});

export default connect(null, mapDispatchToProps)(CollectionItem);
