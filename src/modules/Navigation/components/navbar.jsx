import React from "react";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectCartHidden } from "../../../redux/cart/cart-selectors";
import { selectCurrentUser } from "../../../redux/user/user-selectors";

import { ReactComponent as Logo } from "../assets/crown.svg";
import CartIcon from "./cart-icon";
import CartDropdown from "./cart-dropdown";

import { auth } from "../../Firebase/firebase";

import "../styles/navbar.scss";

const Navbar = ({ currentUser, hidden }) => (
  <nav className="navbar">
    <Link to="/" className="logo-container">
      <Logo className="logo" />
    </Link>
    <div className="options">
      <Link to="/shop" className="option">
        SHOP
      </Link>
      <Link to="/contact" className="option">
        CONTACT
      </Link>
      {currentUser ? (
        <div className="option" onClick={() => auth.signOut()}>
          SIGN OUT
        </div>
      ) : (
        <Link to="/signin" className="option">
          SIGN IN
        </Link>
      )}
      <CartIcon />
    </div>
    {hidden ? null : <CartDropdown />}
  </nav>
);

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser,
  hidden: selectCartHidden,
});

export default connect(mapStateToProps)(Navbar);
