import React from "react";

import "../styles/sign-in.scss";

import FormInput from "./form-input";
import SubmitButton from "./submit-button";

import { auth, signInWithGoogle } from "../../Firebase/firebase";

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    const { email, password } = this.state;

    try {
      await auth.signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.log(error);
    }
    this.setState({ email: "", password: "" });
  };

  render() {
    return (
      <div className="sign-in">
        <h2 className="title">I already have an account.</h2>
        <span>Sign in or use a Google account!</span>

        <form onSubmit={this.handleSubmit}>
          <FormInput
            name="email"
            type="email"
            label="e-mail"
            value={this.state.email}
            handleChange={this.handleChange}
            required
          />
          <FormInput
            name="password"
            type="password"
            label="password"
            value={this.state.password}
            handleChange={this.handleChange}
            required
          />
          <div className="buttons">
            <SubmitButton type="submit">SIGN IN</SubmitButton>
            <SubmitButton
              type="button"
              onClick={signInWithGoogle}
              isGoogleSignIn
            >
              GOOGLE SIGN IN
            </SubmitButton>
          </div>
        </form>
      </div>
    );
  }
}

export default SignIn;
