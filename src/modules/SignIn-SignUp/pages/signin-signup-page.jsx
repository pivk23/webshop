import React from "react";

import { SignIn, SignUp } from "../components";

import "../styles/sign-in-and-sign-up.scss";

const SignInSignUpPage = () => (
  <div className="sign-in-and-sign-up">
    <SignIn />
    <SignUp />
  </div>
);

export default SignInSignUpPage;
