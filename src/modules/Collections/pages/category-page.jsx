import React from "react";

import { connect } from "react-redux";
import { selectCollection } from "../../../redux/shop/shop-selectors";

import CollectionItem from "../../Shop/components/collection-item";

const CollectionPage = ({ collection }) => (
  <div className="category">
    <h2>Collection Page</h2>
  </div>
);

const mapStateToProps = (state, ownProps) => ({
  collection: selectCollection(ownProps.match.params.collectionId)(state),
});

export default connect(mapStateToProps)(CollectionPage);
