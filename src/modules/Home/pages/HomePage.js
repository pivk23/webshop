import React from "react";

import { Directory } from "../components";

const HomePage = () => (
  <>
    <Directory />
  </>
);

export default HomePage;
